$(function(){
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover();
	$('.carousel').carousel({
		interval: 2000
	});

	$('#contacto').on('show.bs.modal', function(e){
		console.log('Se logró mostrar');
		$('#contactoBtn').removeClass('btn-outline-success');
		$('#contactoBtn').addClass('btn-primary');
		$('#contactoBtn').prop('disable', true);
	});
	$('#contacto').on('shown.bs.modal', function(e){
		console.log('Se logró mostrar');
	});
	$('#contacto').on('hide.bs.modal', function(e){
		console.log('El modal se oculta');
	});
	$('#contacto').on('hiden.bs.modal', function(e){
		console.log('El modal se ocultó');
		$('#contactoBtn').prop('disable', false);
	});
});
	